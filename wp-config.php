<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fairsource');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PJ~9SH<^5RaqhY*gC<5KW,AJEMo?zK|,w~3]M n, fKsq]rGl)VXM1c<*@L#D)L=');
define('SECURE_AUTH_KEY',  'V9^Ix9CTTN3=18jZ%)&wiDRp#5Kzh~sz:W!5WpE~=8`0 zVE4o7@R.8;gzHpBdn2');
define('LOGGED_IN_KEY',    'Fd[JavjN^j4SjbY/q<4Q90.!^E>aK%}X7/t,=d4bovnoETwIl+<S|6>+&r4HkS7s');
define('NONCE_KEY',        'qQjN EOqUm)Lp?)8 36Koy+$T3Pf3{W?z1/-Vab25u_m0cdRHMGE~&B;`6V-+AMi');
define('AUTH_SALT',        '{P->oC7(7_9p3txZdReg;J3}K$Dbi(NG@xC1fYtuDOMwRjyzt~v[@Y9-gG:$Ipgk');
define('SECURE_AUTH_SALT', 'jj3H /rVt/@dc1aX@C0[whYoo6nAhH9hP|G)P1,}&MQ?lOjF%@Q]8|<y]dB:8rls');
define('LOGGED_IN_SALT',   'D%cNRzueqTVtw]f50{p7q2|?mq0EHY8<f%b-j]pt[iJ!6|Lj# $F=d_d7-nFdJ f');
define('NONCE_SALT',       ';=*:-5fmAAL&|s~|85LgjE<|Xo7%j~&ZW*T5mcWJKHA]N^l6_{xm,X}>Lqxeb/Si');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
