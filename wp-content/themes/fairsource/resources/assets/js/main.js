/* global DOM */

'use strict';

var AnimationBorder = (function () {
	var init = function () {
		var scroll = window.onscroll;

		window.onscroll = function () {
			if (scroll) {
				scroll();
			}

			handleAnimation(document.getElementById('draw-border'), 'border-active');
			handleAnimation(document.getElementById('business'), 'animation');
			handleAnimation(document.getElementById('government'), 'animation');
			handleAnimation(document.getElementById('education'), 'animation');
		};
	};

	var handleAnimation = function (elem, animationClassname) {
		var isVisible = DOM.isElementInViewport(elem);

		if (isVisible) {
			var isActive = elem.classList.contains(animationClassname);

			if (!isActive) {
				DOM.elementApplyClass(animationClassname, elem, false);
			}
		}
	};

	return {
		init: init,
		handleAnimation: handleAnimation
	};
})();

'use strict';

var DOM = function () {
	var isElementInViewport = function (el) {
		var top = el.offsetTop,
			left = el.offsetLeft,
			width = el.offsetWidth,
			height = el.offsetHeight;

		while (el.offsetParent) {
			el = el.offsetParent;
			top += el.offsetTop;
			left += el.offsetLeft;
		}
		return (
		top >= window.pageYOffset &&
		left >= window.pageXOffset &&
		(top + height) <= (window.pageYOffset + window.innerHeight) + 350 &&
		(left + width) <= (window.pageXOffset + window.innerWidth));
	};

	var elementApplyClass = function (name, element, doRemove) {
		if (typeof element.valueOf() === "string") {
			element = document.getElementById(element);
			return element;
		}
		if (doRemove) {
			element.className = element.className.replace(new RegExp("\\b" + name + "\\b", "g"));
		} else {
			element.className = element.className + " " + name;
		}
	};

	return {
		isElementInViewport: isElementInViewport,
		elementApplyClass: elementApplyClass
	};
}();

/* global DOM */

'use strict';

var Expertise = (function () {

	var elements = [];
	var init = function () {
		elements = Array.prototype.slice.call(document.getElementsByClassName('expertise-item'));
		window.addEventListener('scroll', checkExpertise);
		checkExpertise();
	};

	var checkExpertise = function() {
		if(DOM.isElementInViewport(document.querySelector('#expertise h2'))) {
			window.removeEventListener('scroll', checkExpertise);
			expertiseSlideIn();
		}
	};

	var expertiseSlideIn = function () {
		elements.shift().classList.remove('hidden');
		if (elements.length !== 0) {
			window.setTimeout(expertiseSlideIn, 1000);
		}
	};

	return {
		init: init
	};
})();

/* global XMLHttpRequest */
'use strict';

var Form = (function() {
	var form, submit, name, message, email;
	var init = function() {
		form = document.getElementById('form');
		form.addEventListener('submit', submitForm, true);
		submit = form.getElementsByClassName('contact-button')[0];
		name = form.getElementsByClassName('input-left')[0];
		email = form.getElementsByClassName('input-right')[0];
		message = document.getElementById('message');
	};

	var submitForm = function(evt) {
		if (document.querySelectorAll('#' + form.id + ' :invalid').length) {
			// Let browser validation handle this
			alert('Invalid form');
			return false;
		}
		evt.preventDefault();
		var http = new XMLHttpRequest();
		http.open('POST', form.action, true);
		http.setRequestHeader(
			'Content-type',
			'application/x-www-form-urlencoded'
		);
		var params =
			'email=' +
			encodeURI(email.value) +
			'&name=' +
			encodeURI(name.value) +
			'&message=' +
			encodeURI(message.value);

		submit.disabled = true;
		http.onreadystatechange = function() {
			if (http.readyState !== 4) return;
			submit.disabled = false;
			if (!http.responseText) {
				alert('Failed to send mail: \nTry again');
				return;
			}
			var json = JSON.parse(http.responseText);
			if (json.response === 'succes') {
				alert('Succesfully send mail');
			} else {
				alert('Failed to send mail: \n' + json.message);
			}
		};
		http.send(params);
		return false;
	};

	return {
		init: init
	};
})();

'use strict';

var LoadMore = (function () {

	var all;
	var index = 0;

	var init = function () {
		all = document.getElementById('resource-list').children;
		for (var i = 0; i < all.length; i++) {
			all[i].classList.add('hidden');
		}
		load();
		document.getElementById('loadMore').onclick = load;
	};

	var load = function () {
		index += 4;
		for (var i = 0; i < Math.min(index, all.length); i++) {
			all[i].classList.remove('hidden');
		}
		if (index >= all.length) {
			document.getElementById('loadMore').classList.add('hidden');
		}
	};

	return {
		init: init
	};
})();

'use strict';

var Nav = (function(){
	var init = function() {
		window.addEventListener('scroll', function() {
			shrinkNav();
		});
	};

	var shrinkNav = function() {
		var distanceY = window.pageYOffset || document.documentElement.scrollTop;
		var shrinkOn = 300;
		var header = document.querySelector('.main-nav-outer');
		if (distanceY >= shrinkOn) {
			header.classList.add('smaller');
		} else if (header.classList.contains('smaller')) {
			header.classList.remove('smaller');
		}
	};

	return {
		init: init
	};
})();

'use strict';

var Slider = (function () {

	var slideIndex = 1,
		mouseOver = false,
		active = false,
		slides = document.getElementsByClassName('banner-slides');

	var init = function () {
		window.setInterval(function () {
			showSlides();
		}, 5000);
		mouseClick();
	};

	var mouseClick = function () {
		var slide_show = document.getElementsByClassName('slideshow')[0];
		slide_show.onmouseover = function () {
			mouseOver = true;
		};
		slide_show.onmouseout = function () {
			mouseOver = false;
		};
		slide_show.onclick = function () {
			mouseOver = false;
			showSlides();
		};
	};

	var showSlides = function () {
		if (mouseOver === false && active === false) {
			active = true;
			if (slideIndex > slides.length) {
				slideIndex = 1;
			}
			for (var i = 0; i < slides.length; i++) {
				slides[i].className = 'banner-slides fade';
			}
			slides[slideIndex - 1].className = 'banner-slides fade show';
			if (slideIndex === slides.length) {
				slides[0].className = 'banner-slides fade showbefore';
			} else {
				slides[slideIndex].className = 'banner-slides fade showbefore';
			}
			window.setTimeout(function () {
				active = false;
			}, 1000);
			slideIndex++;
		}
	};
	return {
		init: init
	};
})();

/* global DOM */

'use strict';

var Toggle = (function() {
	var init = function(){
		var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

		if (w < 1024) {
			var elements = document.getElementsByClassName('toggle');

			for(var i = 0; i < elements.length; i++) {
				addToggleToElement(elements[i]);
			}
		}
	};

	var addToggleToElement = function(elm) {
		elm.getElementsByClassName('dots')[0].innerHTML = '...';
		elm.getElementsByClassName('textshow')[0].innerHTML += '<br />Read more';
		elm.getElementsByClassName('textshow')[0].style.cursor='pointer';
		elm.getElementsByClassName('hidden-content')[0].style.display='none';
		elm.getElementsByClassName("textshow")[0].onclick = function() {

			elm.getElementsByClassName('dots')[0].innerHTML = '';
			elm.getElementsByClassName('textshow')[0].innerHTML = '';
			elm.getElementsByClassName('hidden-content')[0].style.display='';
		};
	};

	return {
		init : init
	};
})();

/* global Slider, Expertise, AnimationBorder, LoadMore, Form, Toggle, Nav */

'use strict';

(function () {
	var firstLoad = window.onload;
	window.onload = function () {
		if (firstLoad) firstLoad();
		Slider.init();
		AnimationBorder.init();
		Expertise.init();
		LoadMore.init();
		Form.init();
		Nav.init();
		Toggle.init();
	};
})();

//# sourceMappingURL=main.js.map
