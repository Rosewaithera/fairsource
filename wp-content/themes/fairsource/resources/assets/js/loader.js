/* global Slider, Expertise, AnimationBorder, LoadMore, Form, Toggle, Nav */

'use strict';

(function () {
	var firstLoad = window.onload;
	window.onload = function () {
		if (firstLoad) firstLoad();
		Slider.init();
		AnimationBorder.init();
		Expertise.init();
		LoadMore.init();
		Form.init();
		Nav.init();
		Toggle.init();
	};
})();
