'use strict';

var Slider = (function () {

	var slideIndex = 1,
		mouseOver = false,
		active = false,
		slides = document.getElementsByClassName('banner-slides');

	var init = function () {
		window.setInterval(function () {
			showSlides();
		}, 5000);
		mouseClick();
	};

	var mouseClick = function () {
		var slide_show = document.getElementsByClassName('slideshow')[0];
		slide_show.onmouseover = function () {
			mouseOver = true;
		};
		slide_show.onmouseout = function () {
			mouseOver = false;
		};
		slide_show.onclick = function () {
			mouseOver = false;
			showSlides();
		};
	};

	var showSlides = function () {
		if (mouseOver === false && active === false) {
			active = true;
			if (slideIndex > slides.length) {
				slideIndex = 1;
			}
			for (var i = 0; i < slides.length; i++) {
				slides[i].className = 'banner-slides fade';
			}
			slides[slideIndex - 1].className = 'banner-slides fade show';
			if (slideIndex === slides.length) {
				slides[0].className = 'banner-slides fade showbefore';
			} else {
				slides[slideIndex].className = 'banner-slides fade showbefore';
			}
			window.setTimeout(function () {
				active = false;
			}, 1000);
			slideIndex++;
		}
	};
	return {
		init: init
	};
})();
