'use strict';

var Nav = (function(){
	var init = function() {
		window.addEventListener('scroll', function() {
			shrinkNav();
		});
	};

	var shrinkNav = function() {
		var distanceY = window.pageYOffset || document.documentElement.scrollTop;
		var shrinkOn = 300;
		var header = document.querySelector('.main-nav-outer');
		if (distanceY >= shrinkOn) {
			header.classList.add('smaller');
		} else if (header.classList.contains('smaller')) {
			header.classList.remove('smaller');
		}
	};

	return {
		init: init
	};
})();
