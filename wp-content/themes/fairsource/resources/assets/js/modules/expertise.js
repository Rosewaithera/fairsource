/* global DOM */

'use strict';

var Expertise = (function () {

	var elements = [];
	var init = function () {
		elements = Array.prototype.slice.call(document.getElementsByClassName('expertise-item'));
		window.addEventListener('scroll', checkExpertise);
		checkExpertise();
	};

	var checkExpertise = function() {
		if(DOM.isElementInViewport(document.querySelector('#expertise h2'))) {
			window.removeEventListener('scroll', checkExpertise);
			expertiseSlideIn();
		}
	};

	var expertiseSlideIn = function () {
		elements.shift().classList.remove('hidden');
		if (elements.length !== 0) {
			window.setTimeout(expertiseSlideIn, 1000);
		}
	};

	return {
		init: init
	};
})();
