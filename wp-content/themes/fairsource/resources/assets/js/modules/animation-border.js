/* global DOM */

'use strict';

var AnimationBorder = (function () {
	var init = function () {
		var scroll = window.onscroll;

		window.onscroll = function () {
			if (scroll) {
				scroll();
			}

			handleAnimation(document.getElementById('draw-border'), 'border-active');
			handleAnimation(document.getElementById('business'), 'animation');
			handleAnimation(document.getElementById('government'), 'animation');
			handleAnimation(document.getElementById('education'), 'animation');
		};
	};

	var handleAnimation = function (elem, animationClassname) {
		var isVisible = DOM.isElementInViewport(elem);

		if (isVisible) {
			var isActive = elem.classList.contains(animationClassname);

			if (!isActive) {
				DOM.elementApplyClass(animationClassname, elem, false);
			}
		}
	};

	return {
		init: init,
		handleAnimation: handleAnimation
	};
})();
