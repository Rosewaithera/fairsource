/* global XMLHttpRequest */
'use strict';

var Form = (function() {
	var form, submit, name, message, email;
	var init = function() {
		form = document.getElementById('form');
		form.addEventListener('submit', submitForm, true);
		submit = form.getElementsByClassName('contact-button')[0];
		name = form.getElementsByClassName('input-left')[0];
		email = form.getElementsByClassName('input-right')[0];
		message = document.getElementById('message');
	};

	var submitForm = function(evt) {
		if (document.querySelectorAll('#' + form.id + ' :invalid').length) {
			// Let browser validation handle this
			alert('Invalid form');
			return false;
		}
		evt.preventDefault();
		var http = new XMLHttpRequest();
		http.open('POST', form.action, true);
		http.setRequestHeader(
			'Content-type',
			'application/x-www-form-urlencoded'
		);
		var params =
			'email=' +
			encodeURI(email.value) +
			'&name=' +
			encodeURI(name.value) +
			'&message=' +
			encodeURI(message.value);

		submit.disabled = true;
		http.onreadystatechange = function() {
			if (http.readyState !== 4) return;
			submit.disabled = false;
			if (!http.responseText) {
				alert('Failed to send mail: \nTry again');
				return;
			}
			var json = JSON.parse(http.responseText);
			if (json.response === 'succes') {
				alert('Succesfully send mail');
			} else {
				alert('Failed to send mail: \n' + json.message);
			}
		};
		http.send(params);
		return false;
	};

	return {
		init: init
	};
})();
