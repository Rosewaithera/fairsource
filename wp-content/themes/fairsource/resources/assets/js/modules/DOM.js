'use strict';

var DOM = function () {
	var isElementInViewport = function (el) {
		var top = el.offsetTop,
			left = el.offsetLeft,
			width = el.offsetWidth,
			height = el.offsetHeight;

		while (el.offsetParent) {
			el = el.offsetParent;
			top += el.offsetTop;
			left += el.offsetLeft;
		}
		return (
		top >= window.pageYOffset &&
		left >= window.pageXOffset &&
		(top + height) <= (window.pageYOffset + window.innerHeight) + 350 &&
		(left + width) <= (window.pageXOffset + window.innerWidth));
	};

	var elementApplyClass = function (name, element, doRemove) {
		if (typeof element.valueOf() === "string") {
			element = document.getElementById(element);
			return element;
		}
		if (doRemove) {
			element.className = element.className.replace(new RegExp("\\b" + name + "\\b", "g"));
		} else {
			element.className = element.className + " " + name;
		}
	};

	return {
		isElementInViewport: isElementInViewport,
		elementApplyClass: elementApplyClass
	};
}();
