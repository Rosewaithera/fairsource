'use strict';

var LoadMore = (function () {

	var all;
	var index = 0;

	var init = function () {
		all = document.getElementById('resource-list').children;
		for (var i = 0; i < all.length; i++) {
			all[i].classList.add('hidden');
		}
		load();
		document.getElementById('loadMore').onclick = load;
	};

	var load = function () {
		index += 4;
		for (var i = 0; i < Math.min(index, all.length); i++) {
			all[i].classList.remove('hidden');
		}
		if (index >= all.length) {
			document.getElementById('loadMore').classList.add('hidden');
		}
	};

	return {
		init: init
	};
})();
