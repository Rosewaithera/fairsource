/* global DOM */

'use strict';

var Toggle = (function() {
	var init = function(){
		var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

		if (w < 1024) {
			var elements = document.getElementsByClassName('toggle');

			for(var i = 0; i < elements.length; i++) {
				addToggleToElement(elements[i]);
			}
		}
	};

	var addToggleToElement = function(elm) {
		elm.getElementsByClassName('dots')[0].innerHTML = '...';
		elm.getElementsByClassName('textshow')[0].innerHTML += '<br />Read more';
		elm.getElementsByClassName('textshow')[0].style.cursor='pointer';
		elm.getElementsByClassName('hidden-content')[0].style.display='none';
		elm.getElementsByClassName("textshow")[0].onclick = function() {

			elm.getElementsByClassName('dots')[0].innerHTML = '';
			elm.getElementsByClassName('textshow')[0].innerHTML = '';
			elm.getElementsByClassName('hidden-content')[0].style.display='';
		};
	};

	return {
		init : init
	};
})();
