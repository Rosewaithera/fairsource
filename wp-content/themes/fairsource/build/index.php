<?php

get_header();

get_template_part('views/nav');
get_template_part('views/slider');
get_template_part('views/purpose');
get_template_part('views/whatwedo');
get_template_part('views/expertise');
get_template_part('views/consulting');
get_template_part('views/resources');
get_template_part('views/contact');

get_footer();

?>

