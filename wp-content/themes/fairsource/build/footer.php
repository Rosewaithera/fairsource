<footer class="footer">
	<div class="footer-container">
		<span>Copyright <a href="#" class="footer-copyright-name">Fairsource</a> 2016</span>

		<div class="social-icons">
			<a href="#" class="fa fa-facebook"></a>
			<a href="#" class="fa fa-linkedin"></a>
			<a href="#" class="fa fa-twitter"></a>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>