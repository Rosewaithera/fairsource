<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php the_title_attribute(); ?></title>

	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="description" content="<?php bloginfo('description'); ?>"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Ubuntu:regular,bold,Medium&subset=Latin">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

	<?php wp_head();

	if (WP_DEBUG) {
		echo '<link rel="stylesheet" type="text/css" href="' . get_theme_file_uri("style.css") . '">';
	} else {
		echo '<link rel="stylesheet" type="text/css" href="' . get_theme_file_uri("style.min.css") . '">';
	}
	?>

</head>
<body>
