<?php

header('Content-type: application/json');

$email_template = 'simple.html';

$email = filter_input(INPUT_POST, 'email');
$name = filter_input(INPUT_POST, 'name');
$message = filter_input(INPUT_POST, 'message');

if (empty($name)) {
	$result = array('response' => 'error', 'empty' => 'name', 'message' => '<strong>Error!</strong>&nbsp; Name is empty.');
	echo json_encode($result);
	die;
}

if (empty($email)) {
	$result = array('response' => 'error', 'empty' => 'email', 'message' => '<strong>Error!</strong>&nbsp; Email is empty.');
	echo json_encode($result);
	die;
}

if (empty($message)) {
	$result = array('response' => 'error', 'empty' => 'message', 'message' => '<strong>Error!</strong>&nbsp; Message body is empty.');
	echo json_encode($result);
	die;
}

$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";


$templateTags = array(
	'{{email}}' => htmlentities($email),
	'{{message}}' => htmlentities($message),
	'{{name}}' => htmlentities($name),
);

$templateContents = file_get_contents(dirname(__FILE__) . '/email-templates/' . $email_template);

$contents = strtr($templateContents, $templateTags);

if (@mail($to, $subject, $contents, $headers)) {
	$result = array('response' => 'success', 'message' => 'Your email has been delivered.');
} else {
	$result = array('response' => 'error', 'message' => 'Mail function failed. ' . var_export(error_get_last(), true));
}

echo json_encode($result);

die;
