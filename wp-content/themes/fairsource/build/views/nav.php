<nav class="main-nav-outer">
	<label for="show-menu" class="show-menu">
		<a href="#home"><img class="mobile-logo" src="<?= get_theme_file_uri('resources/assets/images/logo.png'); ?>"></a>
		<img src="<?= get_theme_file_uri('resources/assets/images/hamburger.svg'); ?>"/>
	</label>
	<input type="checkbox" id="show-menu" role="button">
	<ul class="main-nav" id="menu">
		<li><a href="#home"><img class="logo" src="<?= get_theme_file_uri('resources/assets/images/logo.png'); ?>"></a></li>
		<li><a href="#about">About</a></li>
		<li><a href="#expertise">Expertise</a></li>
		<li><a href="#services">Services</a></li>
		<li><a href="#links">Links/Resources</a></li>
		<li><a href="#contact">Contact</a></li>
	</ul>
</nav>