<section class="content-expertise" id="expertise">
	<h2>Expertise</h2>
	<?PHP foreach(get_contents_starting_with('expertise') as $post): ?>
	<div class="expertise-item hidden toggle">
		<h3><?= $post->displaytitle ?></h3>
		<p class="expertise-description"><?= substr($post->content, 0, 200); ?><span class="expertise-description-read-more hidden-content"><?= substr($post->content, 200); ?></span><span class="dots"></span><a class="text-trigger textshow"></a></p>
	</div>
	<?PHP endforeach; ?>
</section>
