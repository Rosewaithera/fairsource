<section class="slideshow">
	<div class="banner-slides fade show">
		<img class="slide-img" src="<?= get_theme_file_uri('resources/assets/images/bannerImgOne.jpg');?>">
		<h2 class="welcome-text">Cras id dui. Aenean ut eros et nisl sagittis vestibulum.</h2>
	</div>
	<div class="banner-slides fade showbefore">
		<img class="slide-img" src="<?= get_theme_file_uri('resources/assets/images/bannerImgTwo.jpg');?>">
		<h2 class="welcome-text">Cras id dui. Aenean ut eros et nisl sagittis vestibulum.</h2>
	</div>
	<div class="banner-slides fade">
		<img class="slide-img" src="<?= get_theme_file_uri('resources/assets/images/bannerImgThree.jpg');?>">
		<h2 class="welcome-text">Cras id dui. Aenean ut eros et nisl sagittis vestibulum.</h2>
	</div>
</section>
