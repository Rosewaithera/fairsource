<section class="whatwedo-outer">
	<div class="whatwedo-content">
		<div class="whatwedo-item">
			<div class="whatwedo-title">
				<?PHP foreach(get_contents_starting_with('whatwedo') as $post): ?>
					<h2><?= $post->displaytitle ?></h2>
				<?PHP endforeach; ?>
			</div>
		</div>
		<div class="whatwedo-item toggle">
			<div class="whatwedo-post">
				<?PHP foreach(get_contents_starting_with('whatwedo') as $post): ?>
					<p class="whatwedo-description"><?= substr($post->content, 0, 200); ?><span class="whatwedo-description-read-more hidden-content"><?= substr($post->content, 200); ?></span><span class="dots"></span><a class="text-trigger textshow"></a></p>
				<?PHP endforeach; ?>
			</div>
		</div>
	</div>
</section>
