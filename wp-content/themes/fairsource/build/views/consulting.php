<section class="section-consulting" id="services">
	<div class="section-content">
		<h2>Consulting Services</h2>

		<div class="section-row toggle" id="business">
			<?= file_get_contents(get_template_directory() . '/resources/assets/images/customer-service.svg') ?>
			<article class="section-row-content">
				<?php foreach (get_contents_starting_with('business') as $post):?>
				<h3>
					<?= $post->displaytitle ?>
				</h3>
					<p class="business-description"><?= substr($post->content, 0, 200); ?><span class="business-description-read-more hidden-content"><?= substr($post->content, 200);?></span><span class="dots"></span><a class="text-trigger textshow"></a></p>
				<?php endforeach; ?>
			</article>
		</div>

		<div class="section-row toggle" id="government">
			<?= file_get_contents(get_template_directory() . '/resources/assets/images/handshake.svg') ?>
			<article class="section-row-content">
				<?php foreach (get_contents_starting_with('government') as $post):?>
				<h3>
					<?= $post->displaytitle ?>
				</h3>
					<p class="consulting-description"><?= substr($post->content, 0, 200); ?><span class="consulting-description-read-more hidden-content"><?= substr($post->content, 200);?></span><span class="dots"></span><a class="text-trigger textshow"></a></p>
				<?php endforeach; ?>
			</article>
		</div>

		<div class="section-row toggle" id="education">
			<?= file_get_contents(get_template_directory() . '/resources/assets/images/education.svg') ?>
			<article class="section-row-content">
				<?php foreach (get_contents_starting_with('education') as $post):?>
				<h3>
					<?= $post->displaytitle ?>
				</h3>
					<p class="education-description"><?= substr($post->content, 0, 200); ?><span class="education-description-read-more hidden-content"><?= substr($post->content, 200);?></span><span class="dots"></span><a class="text-trigger textshow"></a></p>
				<?php endforeach; ?>
			</article>
		</div>
	</div>
</section>
