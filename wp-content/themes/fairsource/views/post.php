<?php
$post = get_post();
$image = get_field('image');
?>
<section class="blogpost">
	<div class="blogpost-top">
		<div class="blogpost-top-breadcrumbs">
			<a href="/">Home</a> &gt; <span><?= $post->post_title ?></span>
		</div>
		<div class="blogpost-top-image" style="background-image: url('<?= $image['url'] ?>')">
		</div>
		<div class="blogpost-top-date">
			<?= get_the_date('j F, Y') ?>
		</div>
		<div class="blogpost-top-title">
			<h1><?= $post->post_title ?></h1>
		</div>
	</div>

	<div class="blogpost-content">
		<?= $post->post_content ?>
	</div>
</section>
