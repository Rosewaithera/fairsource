<section class="contact-outer" id="contact">
	<?php foreach (get_contents_starting_with('contact') as $post): ?>
	<div class="content">
		<h2><?= $post->displaytitle ?></h2>
		<form id="form" class="form-left" name="contact-form" id="contactForm" action="<?= get_theme_file_uri('sendmail.php'); ?>" method="POST">
			<p class="form-title"><?= esc_html($post->meta['form_title'][0]) ?></p>
			<input type="text" name="name" id="name" class="input-left" placeholder="Enter your name here" required>
			<input type="email" name="email" id="email" class="input-right" placeholder="Enter your e-mail address here" required>

			<textarea name="message" id="message" placeholder="What is it you want to have contact about" required></textarea>
			<button type="submit" name="submit" class="contact-button">send</button>
		</form>
		<div class="info-right">
			<p class="info-title"><?= esc_html($post->meta['info_title'][0]) ?></p>
			<p class="info-description"><?= $post->content ?></p>
			<ul class="contact-info">
				<li>
					<i class="fa fa-map-marker"></i>
					<address class="info-detail">
						<?= esc_html($post->meta['address'][0]) ?>
						<br>
						<?= esc_html($post->meta['city'][0]) ?>
					</address>
				</li>
				<li>
					<i class="fa fa-phone"></i>
					<span class="info-detail"><?= esc_html($post->meta['tel'][0]) ?></span>
				</li>
				<li>
					<i class="fa fa-envelope-o"></i>
					<span class="info-detail"><?= esc_html($post->meta['email'][0]) ?></span>
				</li>
			</ul>
		</div>
	</div>
	<?php endforeach; ?>
</section>
