<section class="resources" id="links">
	<div class="resources-title">

		<?PHP foreach (get_contents_starting_with('resources') as $post): ?>
			<h2><?= $post->displaytitle ?></h2>
			<p><?= $post->content ?></p>
		<?PHP endforeach; ?>

	</div>
	<div class="resources-list" id="resource-list">

		<?PHP foreach (get_contents_starting_with('image') as $post): ?>

			<div class="resources-item">
				<a href="<?= get_permalink($post->id) ?>" target="_blank">
					<img src="<?= $post->image['url']; ?>"/>
				</a>
				<div class="resources-item-desc">
					<div class="resources-item-desc-date">
						<?= $post->date ?>
					</div>
					<div class="resources-item-desc-content">
						<?= strip_tags($post->content) ?>
					</div>
				</div>
			</div>

		<?PHP endforeach; ?>

		<?PHP foreach (get_contents_starting_with('pdf') as $pdf): ?>
			<div class="resources-item">
				<a href="<?= $pdf->pdf['url']; ?>" target="_blank">
					<div class="pdf">
						<h3>pdf</h3>
					</div>
					<img src="<?= $pdf->image['url']; ?>"/>
				</a>
				<div class="resources-item-desc">
					<div class="resources-item-desc-date">
						<?= $pdf->date ?>
					</div>
					<div class="resources-item-desc-content">
						<?= strip_tags($pdf->content) ?>
					</div>
				</div>
			</div>
		<?PHP endforeach; ?>
	</div>

	<button id="loadMore" class="loadmore">Load more</button>
</section>
