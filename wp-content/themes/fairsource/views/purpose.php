<div class="purpose-outer" id="about">
	<section class="purpose">

		<div class="purpose-container">
			<img src="<?php echo get_template_directory_uri(); ?>/resources/assets/images/our-purpose.jpg" alt="Purpose"
				 class="image">
			<div class="purpose-text toggle" id="draw-border">
				<?php foreach (get_contents_starting_with('our-purpose') as $post): ?>
					<div class="purpose-content">
						<header>
							<span><?= $post->displaytitle ?></span>
							<img src="<?php echo get_template_directory_uri(); ?>/resources/assets/images/our-purpose.jpg" alt="Purpose" class="purpose-image">

						</header>
						<p class="purpose-description"><?= substr($post->content, 0, 200); ?><span
									class="purpose-description-read-more hidden-content"><?= substr($post->content, 200); ?></span><span
									class="dots"></span><a class="text-trigger textshow"></a></p>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
</div>
