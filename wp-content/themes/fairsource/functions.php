<?php

class OutData
{
	public $id;
	public $content;
	public $displaytitle;
	public $date;
	public $meta = [];
	public $image = "";
	public $pdf = "";
}

	function get_post_by_title($post_title)
	{
		$res = new WP_Query(array('name' => $post_title));
		$out = new outData;
		if ($res->have_posts()) {
			while ($res->have_posts()) {
				$res->the_post();
				$out->id = get_the_id();
				$out->content = get_the_content();
				$out->displaytitle = get_the_title();
				$out->date = get_the_date('j F, Y');
			}
		} else {
			$out->id = -1;
			$out->content = '<p>No posts have been found, add a post with the title "' . htmlentities($post_title) . '" into the wordpress admin</p>';
			$out->date = '' . htmlentities($post_title) . '" into the wordpress admin</p>';
			$out->displaytitle = '';
		}
		return $out;
	}

	function get_contents_starting_with($str)
	{
		$contents = array();
		$res = new WP_Query(array('category_name' => $str));
		if ($res->have_posts()) {
			while ($res->have_posts()) {
				$out = new outData;
				$res->the_post();
				$out->id = get_the_id();
				$out->content = get_the_content();
				$out->displaytitle = get_the_title();
				$out->date = get_the_date('j F, Y');
				$out->meta = get_post_custom();
				if(get_field('image')  )
					$out->image = get_field('image');
				if(get_field('pdf')  )
					$out->pdf = get_field('pdf');
				array_push($contents, $out);
			}
		} else {
			$out = new outData;
			$out->id = -1;
			$out->content = '<p>No posts have been found, add a post with the category "' . htmlentities($str) . '" into the wordpress admin</p>';
			$out->displaytitle = '';
			array_push($contents, $out);
		}
		return $contents;
	}

	// Remove WP EMOJI
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('admin_print_styles', 'print_emoji_styles');

	// enqueue scripts
	function enqueue_scripts_footer() {
		if (WP_DEBUG) {
			wp_enqueue_script( 'script', get_template_directory_uri() . '/resources/assets/js/main.js', array(), 1, true);
		} else {
			wp_enqueue_script( 'script', get_template_directory_uri() . '/resources/assets/js/main.min.js', array(), 1, true);
		}
	}
	add_action('wp_enqueue_scripts', 'enqueue_scripts_footer');
