'use strict';

var gulp = require('gulp');
var pump = require('pump');
var clean = require('gulp-clean');

gulp.task('copy', function() {
	gulp.src('resources/assets/js/main.min.js')
		.pipe(gulp.dest('build/js/'));
	gulp.src('style.min.css')
		.pipe(gulp.dest('build/css/'));
	gulp.src('resources/assets/images/*')
		.pipe(gulp.dest('build/images/'));
	gulp.src('*.php')
		.pipe(gulp.dest('build/'));
	gulp.src('views/*.php')
		.pipe(gulp.dest('build/views/'));
});

gulp.task('copy:clean', function(cb) {
	pump([gulp.src([
		'build/'
	]),
	clean()
	], cb);
});
