'use strict';
var gulp = require('gulp');
var imagemin = require('gulp-imagemin');

gulp.task('image-min', function() {
	gulp.src(['resources/assets/images/*.jpg', 'resources/assets/images/*.png', 'resources/assets/images/*.svg'])
		.pipe(imagemin())
		.pipe(gulp.dest('build/images'))
});
